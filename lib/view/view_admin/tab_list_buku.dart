import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:opac_android_kp/Api/ApiService.dart';
import 'package:opac_android_kp/Class/CacheModel.dart';
import 'package:opac_android_kp/Class/HiveModel.dart';
import 'package:opac_android_kp/Class/Post.dart';
import 'package:opac_android_kp/custom/custom_list_tile.dart';
import 'package:opac_android_kp/view/view_admin/detailScreen.dart';
import 'package:opac_android_kp/view/view_form.dart/createBuku.dart';
import 'package:opac_android_kp/view/view_user/detailScreen3.dart';

class TabListBukuAdmin extends StatefulWidget {
  @override
  _TabListBukuAdminState createState() => _TabListBukuAdminState();
}

class _TabListBukuAdminState extends State<TabListBukuAdmin> {
 final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
   final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();

  ApiService _apiService = ApiService();
  List _postsForDisplay = List();
  TextEditingController editingController = TextEditingController();
  int page = 1;
  int data = 10;

//  kalau data cache dat set true
  bool isCache = false;

  bool isLoading = false;

  bool isVisible = false;

  @override
  void initState() {
    super.initState();
    simpanDataBuku(page);
  }

  Future simpanDataBuku(int page) async {
    HiveModel _hiveModel = HiveModel();
    // List<Datum> _datum;
    String buku = "key Admin";

    try {
      // cek data cache
      var dataCache;
      dataCache = await _hiveModel.getCache(buku);
      // print(dataCache);

      if (dataCache == null ||
          dataCache['lastFetchTime'].isBefore(
              DateTime.now().subtract(dataCache['cacheValidDuration']))) {
        var res = await _apiService.fetchPaginate(page);

        setState(() {
          for (var data in res) {
            _postsForDisplay.add(data);
          }
          // simpan data
          Map<String, dynamic> mapCache = {
            'cacheValidDuration': Duration(minutes: 30).toString(),
            'lastFetchTime': DateTime.now(),
            'data': jsonEncode(_postsForDisplay)
          };
          _hiveModel.addCache(mapCache, buku);
        });
      } else {
        setState(() {
          isCache = true;
          for (var data in dataCache['data']) {
            _postsForDisplay.add(data);
          }
        });
      }
    } catch (e) {
      print("else terakhir asdgr333");
    }
  }

  Future getMoreDataBuku(int page) async {
    print('more');
    HiveModel _hiveModel = HiveModel();
    // List<Datum> _datum;
    String buku = "key Admin";

    try {
      var res = await _apiService.fetchPaginate(page);
      print(res.runtimeType);
      setState(() {
        //        set iscache false
        isCache = false;

        for (var data in res) {
          _postsForDisplay.add(data);
          print(_postsForDisplay.toString());
        }
        // simpan data
        Map<String, dynamic> mapCache = {
          'cacheValidDuration': Duration(minutes: 30).toString(),
          'lastFetchTime': DateTime.now(),
          'data': jsonEncode(_postsForDisplay)
        };

        _hiveModel.addCache(mapCache, buku);
      });
    } catch (e) {
      print("else terakhir asdgr333");
    }
  }

  Future<void> _handleRefresh() {
    HiveModel _hive = HiveModel();
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
    });
    setState(() {
      print("refresh");
      _hive.deleteCache();
      _postsForDisplay.clear();
    });
    return completer.future.then<void>((_) {
      _scaffoldKey.currentState?.showSnackBar(SnackBar(
          content: const Text('Refresh complete'),
          action: SnackBarAction(
              label: 'RETRY',
              onPressed: () {
                _refreshIndicatorKey.currentState.show();
              })));
    });
  }

  Future _loadData() async {
    await Future.delayed(Duration(seconds: 1, milliseconds: 100));
    setState(() {
      isLoading = false;
      page++;
      getMoreDataBuku(page);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: (Color.fromARGB(255, 139, 215, 234)),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => new CreateBuku()));
              },
              tooltip: 'tambah',

              child: Icon(Icons.add),
            ),
            body: Stack(
              children: <Widget>[
                LiquidPullToRefresh(
                    backgroundColor: (Color.fromARGB(255, 139, 215, 234)),
                    color: Colors.white,
                    key: _refreshIndicatorKey,
                    onRefresh: _handleRefresh,
                    showChildOpacityTransition: true,
                    child: Column(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Daftar Buku",
                          style: TextStyle(
                              fontFamily: "LatoBlack",
                              fontSize: 27,
                              color: Colors.white,
                              decoration: TextDecoration.none),
                        ),
                      ),
                      _paginate(),
                    ])),
                Center(
                  child: Container(
                    height: isLoading ? 50 : 0,
                    width: isLoading ? 50 : 0,
                    // color: Colors.black26,
                    child: new CircularProgressIndicator(
                      // backgroundColor: Color.fromARGB(255, 139, 215, 234) ,
                    ),
                  ),
                ),
              ],
            )));
  }

  _paginate() {
    return NotificationListener<ScrollNotification>(
      onNotification: (ScrollNotification scrollInfo) {
        if (!isLoading &&
            scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
          setState(() {
            isLoading = true;
          });
          _loadData();
        }
      },
      child: _list(),
    );
  }

  _list() {
    return Expanded(
        child: _postsForDisplay != null
            ? ListView.builder(
                cacheExtent: 10.0,
                padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 10.0),
                itemCount: _postsForDisplay.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: _listtile(index),
                    // child: _listtiles()
                  );
                })
            : _circularProcces());
  }

  _circularProcces() {
    return Column(
      children: <Widget>[
        Container(
            height: 110,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/buku.GIF"),
                fit: BoxFit.cover,
              ),
            )),
        Text(
          "Please Wait",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ],
    );
    // Align(
    //   alignment: Alignment.topCenter,
    //   child: CircularProgressIndicator(),
    // );
  }

  _listtile(index) {
    String kosong = "-";
    Datum datum;
    String typeData = _postsForDisplay[index].runtimeType.toString();
    print(_postsForDisplay[index]);
    if (typeData == "_InternalLinkedHashMap<String, dynamic>") {
      datum = Datum.fromJson(_postsForDisplay[index]);
    } else {
      datum = _postsForDisplay[index];
    }

    return Column(
      children: <Widget>[
        Material(
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new DetailScreen(
                        datums: datum
                      )));
            },
            splashColor: Colors.grey,
            child: CustomListTile(
              judul: datum.judul == null ? kosong : datum.judul,
              pengarang: datum.pengarang == null ? kosong : datum.pengarang,
              subjek: datum.tajukSubjek == null ? kosong : datum.penerbit,
            ),
          ),
        ),
      ],
    );
  }
}
