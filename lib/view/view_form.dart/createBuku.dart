// import 'dart:html';

import 'dart:async';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:opac_android_kp/Api/ApiService.dart';
import 'package:opac_android_kp/Class/Post.dart';

final format = DateFormat("yyyy");

class CreateBuku extends StatefulWidget {
  @override
  _CreateBukuState createState() => _CreateBukuState();
}

class _CreateBukuState extends State<CreateBuku> {
  bool isLoading = false;
  final _formField = GlobalKey<FormState>();
  ApiService _apiService = new ApiService();
  final FocusNode _focusjudul = FocusNode();
  final FocusNode _focuspengarang = FocusNode();
  final FocusNode _focusnoIndukBuku = FocusNode();
  final FocusNode _focuscallNumber1 = FocusNode();
  final FocusNode _focuscallNumber2 = FocusNode();
  final FocusNode _focuscallNumber3 = FocusNode();
  final FocusNode _focustajukSubjek = FocusNode();
  final FocusNode _focusjilidKe = FocusNode();
  final FocusNode _focusseri = FocusNode();
  final FocusNode _focusedisiKe = FocusNode();
  final FocusNode _focuscetakanKe = FocusNode();
  final FocusNode _focuspenerbit = FocusNode();
  final FocusNode _focuskotaTerbit = FocusNode();
  final FocusNode _focustahunTerbit = FocusNode();
  final FocusNode _focusjumlahHalaman = FocusNode();
  final FocusNode _focusilustrasi = FocusNode();
  final FocusNode _focusbibliografi = FocusNode();
  final FocusNode _focusisbn = FocusNode();
  final FocusNode _focustinggiBuku = FocusNode();
  final FocusNode _focusditerimaDari = FocusNode();
  final FocusNode _focusjumlahEksemplar = FocusNode();
  final FocusNode _focusbuttonsimpan = FocusNode();

  TextEditingController judul = new TextEditingController();
  TextEditingController pengarang = new TextEditingController();
  TextEditingController noIndukBuku = new TextEditingController();
  TextEditingController callNumber1 = new TextEditingController();
  TextEditingController callNumber2 = new TextEditingController();
  TextEditingController callNumber3 = new TextEditingController();
  TextEditingController tajukSubjek = new TextEditingController();
  TextEditingController jilidKe = new TextEditingController();
  TextEditingController seri = new TextEditingController();
  TextEditingController edisiKe = new TextEditingController();
  TextEditingController cetakanKe = new TextEditingController();
  TextEditingController penerbit = new TextEditingController();
  TextEditingController kotaTerbit = new TextEditingController();
  TextEditingController tahunTerbit = new TextEditingController();
  TextEditingController jumlahHalaman = new TextEditingController();
  TextEditingController ilustrasi = new TextEditingController();
  TextEditingController bibliografi = new TextEditingController();
  TextEditingController isbn = new TextEditingController();
  TextEditingController tinggiBuku = new TextEditingController();
  TextEditingController diterimaDari = new TextEditingController();
  TextEditingController jumlahEksemplar = new TextEditingController();
  TextEditingController selesaiDiproses = new TextEditingController();

  String _judul = "";
  String _pengarang = "";
  String _noIndukBuku = "";
  String _callNumber1 = "";
  String _callNumber2 = "";
  String _callNumber3 = "";
  String _tajukSubjek = "";
  String _jilidKe = "";
  String _seri = "";
  String _edisiKe = "";
  String _cetakanKe = "";
  String _penerbit = "";
  String _kotaTerbit = "";
  String _tahunTerbit = "";
  String _jumlahHalaman = "";
  String _ilustrasi = "";
  String _bibliografi = "";
  String _isbn = "";
  String _tinggiBuku = "";
  String _diterimaDari = "";
  String _jumlahEksemplar = "";

  void createData() {
    if (_formField.currentState.validate()) {
      int _id;
      _judul = judul.text.toString();
      _pengarang = pengarang.text.toString();
      _noIndukBuku = noIndukBuku.text.toString();
      _callNumber1 = callNumber1.text.toString();
      _callNumber2 = callNumber2.text.toString();
      _callNumber3 = callNumber3.text.toString();
      _tajukSubjek = tajukSubjek.text.toString();
      _jilidKe = jilidKe.text.toString();
      _seri = seri.text.toString();
      _edisiKe = edisiKe.text.toString();
      _cetakanKe = cetakanKe.text.toString();
      _penerbit = penerbit.text.toString();
      _kotaTerbit = kotaTerbit.text.toString();
      _tahunTerbit = tahunTerbit.text.toString();
      _jumlahHalaman = jumlahHalaman.text.toString();
      _ilustrasi = ilustrasi.text.toString();
      _bibliografi = bibliografi.text.toString();
      _isbn = isbn.text.toString();
      _tinggiBuku = tinggiBuku.text.toString();
      _diterimaDari = diterimaDari.text.toString();
      _jumlahEksemplar = jumlahEksemplar.text.toString();
      DateTime _selesaiDiproses = DateTime.now();

      Datum data = Datum(
        id: _id,
        noIndukBuku: _noIndukBuku,
        judul: _judul,
        pengarang: _pengarang,
        penerbit: _penerbit,
        callNumber1: _callNumber1,
        callNumber2: _callNumber2,
        callNumber3: _callNumber3,
        tajukSubjek: _tajukSubjek,
        jilidKe: _jilidKe,
        seri: _seri,
        edisiKe: _edisiKe,
        cetakanKe: _cetakanKe,
        kotaTerbit: _kotaTerbit,
        tahunTerbit: _tahunTerbit,
        jumlahHalaman: _jumlahHalaman,
        ilustrasi: _ilustrasi,
        bibliografi: _bibliografi,
        isbn: _isbn,
        tinggiBuku: _tinggiBuku,
        diterimaDari: _diterimaDari,
        jumlahEksemplar: _jumlahEksemplar,
        selesaiDiproses: _selesaiDiproses,
      );
      _apiService.createBuku(data).then((isSuccess) {
        setState(() {
          if (isSuccess) {
            print("input data berhasil");
            print(datumToJson(data).toString());
            _sweetAlertSuccess();
          } else {
            _sweetAlertError();
            // showDialog(context: context, child: Text("gagal input text"));
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        title: Text("Tambah Data Buku"),
      ),
      backgroundColor: Colors.lightBlueAccent,
      body: Container(
         decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9.0),
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 161, 211, 255),
                Color.fromARGB(255, 122, 193, 255),
                Color.fromARGB(255, 82, 174, 255),
              ],
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
            ),
          ),
        padding: const EdgeInsets.all(27.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Form(
              key: _formField,
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10),
                    child: _fieldnoIndukBuku(context),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldJudul(context),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldPengarang(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldPenerbit(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldKotaTerbit(),
                  ),
                  Text("Letak Rak :"),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: _fieldcallNumber1(),
                        ),
                        Flexible(
                          child: _fieldcallNumber2(),
                        ),
                        Flexible(
                          child: _fieldcallNumber3(),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldSubjek(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldTahunTerbit(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldCetakanKe(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldJumlahEksemplar(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: _fieldjilidKe(),
                        ),
                        Flexible(
                          child: _fieldEdisiKe(),
                        ),
                        Flexible(
                          child: _fieldSeri(),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: _fieldJumlahHalaman(),
                  ),
                  Visibility(child: _circularProcces(), visible: isLoading),
                  Padding(
                    padding: const EdgeInsets.only(top: 25.0),
                    child: _btnSimpan(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _circularProcces() {
    return Align(
      alignment: Alignment.topCenter,
      child: CircularProgressIndicator(),
    );
  }

  _sweetAlertSuccess() {
    isLoading = false;
    return SweetAlert.show(
      context,
      title: "Berhasil",
      subtitle: "menambahkan data",
      style: SweetAlertStyle.success,
       onPress: (isConfirm) {
        Navigator.of(context).pop();
      },
    );
  }

  _sweetAlertError() {
    return SweetAlert.show(
      context,
      title: "Gagal",
      subtitle: "menambahkan data",
      style: SweetAlertStyle.error,
    );
  }

  void _alertSuccess() {
    AlertDialog alertD = new AlertDialog(
      content: Column(
        children: <Widget>[
          new Text("Create Data Successful"),
        ],
      ),
    );
    showDialog(
        context: context,
        child: alertD,
        builder: (context) {
          Timer(Duration(seconds: 3), () => Navigator.of(context).pop());
        });
  }

  String validasiNoInduk(String value) {
    if (value.isEmpty) {
      return 'no induk buku cannot be empty';
    }
  }

  String validasiJudul(String value) {
    if (value.isEmpty) {
      return 'Judul cannot be empty';
    }
  }

  _fieldnoIndukBuku(BuildContext context) {
    return TextFormField(
      controller: noIndukBuku,
      validator: validasiNoInduk,
      onSaved: (String val) {
        _noIndukBuku = val;
      },
      textInputAction: TextInputAction.next,
      focusNode: _focusnoIndukBuku,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focusnoIndukBuku, _focusjudul);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          prefixIcon: Icon(Icons.looks_one),
          labelText: "No Induk Buku",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldJudul(BuildContext context) {
    return TextFormField(
      controller: judul,
      validator: validasiJudul,
      onSaved: (String val) {
        _judul = val;
      },
      keyboardType: TextInputType.multiline,
      maxLines: null,
      textInputAction: TextInputAction.next,
      focusNode: _focusjudul,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focusjudul, _focuspengarang);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          prefixIcon: Icon(Icons.text_fields),
          labelText: "Judul Buku",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldPengarang() {
    return TextFormField(
      controller: pengarang,
      textInputAction: TextInputAction.next,
      focusNode: _focuspengarang,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focuspengarang, _focuspenerbit);
      },
      keyboardType: TextInputType.multiline,
      maxLines: null,
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          prefixIcon: Icon(Icons.person),
          labelText: "Pengarang",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldPenerbit() {
    return TextFormField(
      controller: penerbit,
      focusNode: _focuspenerbit,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focuspenerbit, _focuskotaTerbit);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Penerbit",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldKotaTerbit() {
    return TextFormField(
      controller: kotaTerbit,
      focusNode: _focuskotaTerbit,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focuskotaTerbit, _focuscallNumber1);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Kota Terbit",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldcallNumber1() {
    return TextFormField(
      controller: callNumber1,
      focusNode: _focuscallNumber1,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focuscallNumber1, _focuscallNumber2);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Rak 1",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldcallNumber2() {
    return TextFormField(
      controller: callNumber2,
      focusNode: _focuscallNumber2,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focuscallNumber2, _focuscallNumber3);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Rak 2",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldcallNumber3() {
    return TextFormField(
      controller: callNumber3,
      focusNode: _focuscallNumber3,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focuscallNumber3, _focustajukSubjek);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Rak 3",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldSubjek() {
    return TextFormField(
      controller: tajukSubjek,
      focusNode: _focustajukSubjek,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focustajukSubjek, _focustahunTerbit);
      },
      keyboardType: TextInputType.multiline,
      maxLines: null,
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Tajuk Subjek",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldTahunTerbit() {
    return DateTimeField(
      format: format,
      onShowPicker: (context, currentValue) {
        return showDatePicker(
            initialDatePickerMode: DatePickerMode.year,
            context: context,
            initialDate: currentValue ?? DateTime.now(),
            firstDate: DateTime(2000),
            lastDate: DateTime.now());
      },
      controller: tahunTerbit,
      focusNode: _focustahunTerbit,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focustahunTerbit, _focuscetakanKe);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Tahun Terbit",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldCetakanKe() {
    return TextFormField(
      controller: cetakanKe,
      focusNode: _focuscetakanKe,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focuscetakanKe, _focusjumlahEksemplar);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Cetakan ke",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldJumlahEksemplar() {
    return TextFormField(
      controller: jumlahEksemplar,
      focusNode: _focusjumlahEksemplar,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focusjumlahEksemplar, _focusjilidKe);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Jumlah Eksemplar",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldjilidKe() {
    return TextFormField(
      controller: jilidKe,
      focusNode: _focusjilidKe,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focusjilidKe, _focusedisiKe);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Jilid ke",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldEdisiKe() {
    return TextFormField(
      controller: edisiKe,
      focusNode: _focusedisiKe,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focusedisiKe, _focusseri);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Edisi ke",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldSeri() {
    return TextFormField(
      controller: seri,
      focusNode: _focusseri,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focusseri, _focusjumlahHalaman);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Seri ke",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _fieldJumlahHalaman() {
    return TextFormField(
      controller: jumlahHalaman,
      focusNode: _focusjumlahHalaman,
      onFieldSubmitted: (term) {
        _fieldFocusChanged(context, _focusjumlahHalaman, _focusbuttonsimpan);
      },
      decoration: InputDecoration(
          fillColor: const Color(0xfff0f4f8),
          labelText: "Jumlah Halaman",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  _btnSimpan() {
    return Padding(
      padding: EdgeInsets.only(left: 55.0, right: 55.0, bottom: 15.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 50,
        child: Container(
          child: Material(
            borderRadius: BorderRadius.circular(9.0),
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(9.0),
              splashColor: Colors.grey,
              onTap: () {
                _submit();
              },
              child: Center(
                child: Text(
                  "Simpan",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w700),
                ),
              ),
            ),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9.0),
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 161, 211, 255),
                Color.fromARGB(255, 122, 193, 255),
                Color.fromARGB(255, 82, 174, 255),
              ],
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
            ),
          ),
        ),
      ),
    );
  }

  void _submit() async {
    if (_formField.currentState.validate()) {
      setState(() {
        isLoading = true;
      });
      createData();
    } else {
      FocusScope.of(context).requestFocus(_focusnoIndukBuku);
    }
  }

  void _fieldFocusChanged(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
