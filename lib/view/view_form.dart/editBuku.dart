import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:opac_android_kp/Api/ApiService.dart';
import 'package:opac_android_kp/Class/Post.dart';
import 'package:opac_android_kp/Class/Post.dart';
import 'package:sweetalert/sweetalert.dart';

final format = DateFormat("yyyy");

class EditBuku extends StatefulWidget {
  final Datum bk;
  final int id;

  EditBuku({this.id, this.bk});

  @override
  _EditBukuState createState() => _EditBukuState();
}

class _EditBukuState extends State<EditBuku> {
  // BukuClass bukuClass = BukuClass();
  ApiService _apiService = new ApiService();

  TextEditingController id = new TextEditingController();
  TextEditingController noIndukBuku = new TextEditingController();
  TextEditingController judul = new TextEditingController();
  TextEditingController pengarang = new TextEditingController();
  TextEditingController callNumber1 = new TextEditingController();
  TextEditingController callNumber2 = new TextEditingController();
  TextEditingController callNumber3 = new TextEditingController();
  TextEditingController tajukSubjek = new TextEditingController();
  TextEditingController jilidKe = new TextEditingController();
  TextEditingController seri = new TextEditingController();
  TextEditingController edisiKe = new TextEditingController();
  TextEditingController cetakanKe = new TextEditingController();
  TextEditingController penerbit = new TextEditingController();
  TextEditingController kotaTerbit = new TextEditingController();
  TextEditingController tahunTerbit = new TextEditingController();
  TextEditingController jumlahHalaman = new TextEditingController();
  TextEditingController ilustrasi = new TextEditingController();
  TextEditingController bibliografi = new TextEditingController();
  TextEditingController isbn = new TextEditingController();
  TextEditingController tinggiBuku = new TextEditingController();
  TextEditingController diterimaDari = new TextEditingController();
  TextEditingController jumlahEksemplar = new TextEditingController();
  TextEditingController selesaiDiproses = new TextEditingController();

  void editData() {
    int _id = this.widget.id;
    String _noIndukBuku = widget.bk.noIndukBuku;
    String _judul = judul.text.toString();
    String _pengarang = pengarang.text.toString();
    String _callNumber1 = callNumber1.text.toString();
    String _callNumber2 = callNumber2.text.toString();
    String _callNumber3 = callNumber3.text.toString();
    String _tajukSubjek = tajukSubjek.text.toString();
    String _jilidKe = jilidKe.text.toString();
    String _seri = seri.text.toString();
    String _edisiKe = edisiKe.text.toString();
    String _cetakanKe = cetakanKe.text.toString();
    String _penerbit = penerbit.text.toString();
    String _kotaTerbit = kotaTerbit.text.toString();
    String _tahunTerbit = tahunTerbit.text.toString();
    String _jumlahHalaman = jumlahHalaman.text.toString();
    String _ilustrasi = ilustrasi.text.toString();
    String _bibliografi = bibliografi.text.toString();
    String _isbn = isbn.text.toString();
    String _tinggiBuku = tinggiBuku.text.toString();
    String _diterimaDari = diterimaDari.text.toString();
    String _jumlahEksemplar = jumlahEksemplar.text.toString();
    DateTime _selesaiDiproses = DateTime.now();
    DateTime _updatedAt = DateTime.now();

    Datum data = Datum(
        id: _id,
        noIndukBuku: _noIndukBuku,
        judul: _judul,
        pengarang : _pengarang,
        callNumber1 : _callNumber1,
        callNumber2 : _callNumber2,
        callNumber3 : _callNumber3,
        tajukSubjek : _tajukSubjek,
        jilidKe : _jilidKe,
        seri : _seri,
        edisiKe : _edisiKe,
        cetakanKe : _cetakanKe,
        penerbit : _penerbit,
        kotaTerbit : _kotaTerbit,
        tahunTerbit : _tahunTerbit,
        jumlahHalaman : _jumlahHalaman,
        ilustrasi : _ilustrasi,
        bibliografi : _bibliografi,
        isbn : _isbn,
        tinggiBuku : _tinggiBuku,
        diterimaDari : _diterimaDari,
        jumlahEksemplar : _jumlahEksemplar,
        selesaiDiproses: _selesaiDiproses);

    _apiService.updateBuku(data, widget.id).then((isSuccess) {
      if (mounted) {
        setState(() {
          if (isSuccess) {
            print("edit data berhasil");
            _sweetAlertSuccess();
            // _apiService.fetchDetail(widget.id);
            print(datumToJson(data).toString());
          } else {
            print("edit data gagal");
            _sweetAlertError();
          }
        });
      }
    });
  }

  @override
  void initState() {
    id = this.id;
    noIndukBuku = new TextEditingController(text: widget.bk.noIndukBuku);
    judul = new TextEditingController(text: widget.bk.judul);
    pengarang = new TextEditingController(text:widget.bk.pengarang);
    callNumber1 = new TextEditingController(text:widget.bk.callNumber1);
    callNumber2 = new TextEditingController(text:widget.bk.callNumber2);
    callNumber3 = new TextEditingController(text:widget.bk.callNumber3);
    tajukSubjek = new TextEditingController(text:widget.bk.tajukSubjek);
    jilidKe = new TextEditingController(text:widget.bk.jilidKe);
    seri = new TextEditingController(text:widget.bk.seri);
    edisiKe = new TextEditingController(text:widget.bk.edisiKe);
    cetakanKe = new TextEditingController(text:widget.bk.cetakanKe);
    penerbit = new TextEditingController(text:widget.bk.penerbit);
    kotaTerbit = new TextEditingController(text:widget.bk.kotaTerbit);
    tahunTerbit = new TextEditingController(text:widget.bk.tahunTerbit);
    jumlahHalaman = new TextEditingController(text:widget.bk.jumlahHalaman);
    ilustrasi = new TextEditingController(text:widget.bk.ilustrasi);
    bibliografi = new TextEditingController(text:widget.bk.bibliografi);
    isbn = new TextEditingController(text:widget.bk.isbn);
    tinggiBuku = new TextEditingController(text:widget.bk.tinggiBuku);
    diterimaDari = new TextEditingController(text:widget.bk.diterimaDari);
    jumlahEksemplar = new TextEditingController(text:widget.bk.jumlahEksemplar);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text("Edit Data Buku"),
      ),
      backgroundColor: Colors.lightBlueAccent,
      body: Container(
         decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9.0),
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 82, 174, 255),
                Color.fromARGB(255, 122, 193, 255),
                Color.fromARGB(255, 161, 211, 255),
              ],
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
            ),
          ),
        padding: const EdgeInsets.all(27.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
              children: <Widget>[
                _fieldnoIndukBuku(),
                _fieldJudul(),
                _fieldPengarang(),
                _fieldcallNumber1(),
                _fieldcallNumber2(),
                _fieldcallNumber3(),
                _fieldSubjek(),
                _fieldJumlahHalaman(),
                _fieldPenerbit(),
                _fieldKotaTerbit(),
                _fieldTahunTerbit(),
                _fieldjilidKe(),
                _fieldISBN(),
                _fieldEdisiKe(),
                _fieldCetakanKe(),
                _fieldJumlhEksemplar(),
                _fieldTinggiBuku(),
                _btnSimpan(),
                // Visibility(child: _circularProcces(), visible: isLoading),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _sweetAlertSuccess() {
    return SweetAlert.show(
      context,
      title: "Berhasil",
      subtitle: "mengedit data",
      style: SweetAlertStyle.success,
      onPress: (isConfirm) {
        Navigator.of(context).pop();
      },
    );
  }

  _sweetAlertError() {
    return SweetAlert.show(
      context,
      title: "Gagal",
      subtitle: "mengedit data",
      style: SweetAlertStyle.error,
    );
  }

  _circularProcces() {
    return Align(
      alignment: Alignment.topCenter,
      child: CircularProgressIndicator(),
    );
  }

  _fieldnoIndukBuku() {
    return TextFormField(
      controller: noIndukBuku,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "noIndukBuku"),
    );
  }

  _fieldcallNumber1() {
    return TextFormField(
      controller: callNumber1,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Rak 1"),
    );
  }

  _fieldcallNumber2() {
    return TextFormField(
      controller: callNumber2,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Rak 2"),
    );
  }

  _fieldcallNumber3() {
    return TextFormField(
      controller: callNumber3,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Rak 3"),
    );
  }

  _fieldSubjek() {
    return TextFormField(
      controller: tajukSubjek,
       keyboardType: TextInputType.multiline,
      maxLines: null,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Tajuk Subjek"),
    );
  }

  _fieldPengarang() {
    return TextFormField(
      controller: pengarang,
      // onSaved: (e) => password = e,
      decoration: InputDecoration(
        labelText: "Pengarang",
      ),
    );
  }

  _fieldJudul() {
    return TextFormField(
      controller: judul,
       keyboardType: TextInputType.multiline,
      maxLines: null,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Judul Buku"),
    );
  }

  _fieldjilidKe() {
    return TextFormField(
      controller: jilidKe,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "jilid Ke"),
    );
  }

  _fieldSeri() {
    return TextFormField(
      controller: seri,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Seri Ke"),
    );
  }

  _fieldEdisiKe() {
    return TextFormField(
      controller: edisiKe,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Edisi Ke"),
    );
  }

  _fieldCetakanKe() {
    return TextFormField(
      controller: cetakanKe,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Cetakan Ke"),
    );
  }

  _fieldPenerbit() {
    return TextFormField(
      controller: penerbit,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Penerbit Buku"),
    );
  }

  _fieldKotaTerbit() {
    return TextFormField(
      controller: kotaTerbit,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Kota Terbit"),
    );
  }

  _fieldTahunTerbit() {
    return DateTimeField(
      format: format,
      onShowPicker: (context, currentValue) {
        return showDatePicker(
            context: context,
            initialDate: currentValue ?? DateTime.now(),
            firstDate: DateTime(2000),
            lastDate: DateTime.now());
      },
      controller: tahunTerbit,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Tahun Terbit"),
    );
  }

  _fieldJumlahHalaman() {
    return TextFormField(
      controller: jumlahHalaman,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Jumlah Halaman"),
    );
  }

  _btnSimpan() {
     return Padding(
      padding: EdgeInsets.only(
        top: 55.0,
        left: 55.0,
        right: 55.0,
        bottom: 15.0
      ),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 50,
        child: Container(
          child: Material(
            borderRadius: BorderRadius.circular(9.0),
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(9.0),
              splashColor: Colors.grey,
              onTap: () {
                editData();
              },
              child: Center(
                child: Text(
                  "Simpan",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w700),
                ),
              ),
            ),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9.0),
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 161, 211, 255),
                Color.fromARGB(255, 122, 193, 255),
                Color.fromARGB(255, 82, 174, 255),
              ],
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
            ),
          ),
        ),
      ),
    );
   
  }

   _fieldISBN() { 
    return TextFormField(
      controller: isbn,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "ISBN"),
    );}

  _fieldJumlhEksemplar() {
     return TextFormField(
      controller: jumlahEksemplar,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Jumlah Eksemplar"),
    );
  }

  _fieldTinggiBuku() {
     return TextFormField(
      controller: tinggiBuku,

      // onSaved: (e) => email = e,
      decoration: InputDecoration(labelText: "Tinggi Buku"),
    );
  }
}
