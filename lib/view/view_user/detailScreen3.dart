import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:opac_android_kp/Api/ApiService.dart';
import 'package:opac_android_kp/Class/Post.dart';
import 'package:opac_android_kp/custom/custom_vertical_list.dart';
import 'package:toast/toast.dart';

class DetailScreen3 extends StatefulWidget {
  Datum datums;
  DetailScreen3({this.datums});

  @override
  _DetailScreen3State createState() => _DetailScreen3State();
}

class _DetailScreen3State extends State<DetailScreen3> {
  
  String kosong = "-";
  ApiService _apiService = ApiService();
  Datum datum;
  List<Datum> _bukuTerkait = List<Datum>();
  bool isCache = false;
  static const TextStyle _textcard = TextStyle(
    fontSize: 15.0,
    fontFamily: "Poppins",
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );
  static const TextStyle _textcardbesar = TextStyle(
      fontSize: 20.0,
      fontWeight: FontWeight.bold,
      color: Colors.white,
      fontFamily: "Poppins",
      shadows: [
        Shadow(blurRadius: 10.0, color: Colors.blue, offset: Offset(5.0, 5.0))
      ]);

  @override
  void initState() {
    datum = widget.datums;
    print(datum.id);

    _apiService.fetcBukuTerkait(datum.id).then((value) {
      setState(() {
        _bukuTerkait.addAll(value);
      });
    });

    // TODO: implement initState
    super.initState();
  }

  // @override
  // void initState() {
  //   id = widget.idBuku;
  //   setState(() {
  //     _apiService.fetchDetail(id).then((value) {
  //       datum = value;
  //     });
  //     _apiService.fetcBukuTerkait(id).then((value) {
  //       _bukuTerkait.addAll(value);
  //     });
  //   });

  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Detail Buku",
            style: TextStyle(
              fontFamily: "LatoBlack",
            )),
        centerTitle: true,
        backgroundColor: Color.fromARGB(255, 139, 215, 234),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
      ),
      body: Stack(
        children: <Widget>[
          _gambar(),
          ListView(
            children: <Widget>[
              Container(
                height: 150,
                color: Colors.transparent,
              ),
              Stack(
                children: <Widget>[
                  Card(
                      shadowColor: Colors.black,
                      margin:
                          EdgeInsets.only(top: 37.0, right: 10.0, left: 10.0),
                      color: Color.fromARGB(255, 139, 215, 234),
                      shape: new RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey),
                          borderRadius:
                              BorderRadius.all(Radius.circular(20.0))),
                      child: _details(datum)),
                  Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.only(left: 30.0),
                    child: Card(
                        color: Colors.white,
                        shape: new RoundedRectangleBorder(
                            side: BorderSide(color: Colors.pink),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0))),
                        child: Padding(
                            padding: const EdgeInsets.all(18.0),
                            child: Text(" ${datum.callNumber1 ?? kosong} "
                                " ${datum.callNumber2 ?? kosong}  "
                                " ${datum.callNumber3 ?? kosong} "))),
                  ),
                ],
              ),
              Container(
                color: Colors.transparent,
                height: 50.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, bottom: 10),
                child: Text(
                  "Buku Terkait",
                  style: TextStyle(
                    fontSize: 25.0,
                    fontFamily: "LatoBlack",
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(height: 250, child: _listBukuTerkait(datum)),
              Container(
                height: 50,
              )
            ],
          ),
        ],
      ),
    );
  }

  _gambar() {
    return Container(
      height: MediaQuery.of(context).size.width / 2,
      width: MediaQuery.of(context).size.width,
      decoration: new BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          image: new DecorationImage(
              image: new AssetImage("images/cover_detail.JPG"),
              fit: BoxFit.cover)),
    );
  }

  _circularProcces() {
    return Align(
      alignment: Alignment.topCenter,
      child: CircularProgressIndicator(
        backgroundColor: (Colors.deepOrangeAccent),
      ),
    );
  }

  _details(Datum bk) {
    String kosong = " - ";
    return Padding(
      padding: EdgeInsets.only(left: 4.0, right: 4.0),
      child: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                height: 30.0,
              ),
              Text(" ${bk.noIndukBuku ?? kosong} ", style: _textcard),
              Text(
                "${bk.judul ?? kosong}",
                style: _textcardbesar,
                textAlign: TextAlign.right,
              ),
              Text(
                "Pengarang : ${bk.pengarang ?? kosong}",
                style: _textcard,
                textAlign: TextAlign.right,
              ),
              Container(
                height: 30.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(Icons.turned_in_not, color: Colors.white),
                      new Text(" ${bk.tajukSubjek ?? kosong}",
                          overflow: TextOverflow.visible,
                          style: _textcard,
                          textAlign: TextAlign.center),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(Icons.book, color: Colors.white),
                      Column(
                        children: <Widget>[
                          new Text(" ${bk.jumlahHalaman ?? kosong} lembar",
                              overflow: TextOverflow.fade,
                              style: _textcard,
                              textAlign: TextAlign.center),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Divider(
                color: Colors.grey,
                thickness: 2.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        new Text("Penerbit", style: _textcard),
                        new Text(" ${bk.penerbit ?? kosong} ",
                            style: _textcard, textAlign: TextAlign.center)
                      ],
                    ),
                  ),
                  Container(
                    width: 1,
                    height: 30,
                    color: const Color(0xffffffff),
                  ),
                  Flexible(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        new Text("Kota Terbit", style: _textcard),
                        new Text(" ${bk.kotaTerbit ?? kosong}",
                            style: _textcard, textAlign: TextAlign.center),
                      ],
                    ),
                  ),
                  Container(
                    width: 1,
                    height: 30,
                    color: const Color(0xffffffff),
                  ),
                  Flexible(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        new Text("Tahun Terbit", style: _textcard),
                        new Text(" ${bk.tahunTerbit ?? kosong}",
                            style: _textcard, textAlign: TextAlign.center),
                      ],
                    ),
                  ),
                ],
              ),
              Divider(
                color: Colors.grey,
                thickness: 2.0,
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
//            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(children: <Widget>[
                new Icon(Icons.book, color: Colors.white),
                new Text("Jilid : ${bk.jilidKe ?? kosong}",
                    style: _textcard, textAlign: TextAlign.center),
              ]),
              Row(children: <Widget>[
                new Icon(Icons.book, color: Colors.white),
                new Text("ISBN : ${bk.isbn ?? kosong}",
                    style: _textcard, textAlign: TextAlign.center),
              ]),
              Row(children: <Widget>[
                new Icon(Icons.book, color: Colors.white),
                new Text("Edisi ke : ${bk.edisiKe ?? kosong}",
                    style: _textcard, textAlign: TextAlign.center),
              ]),
              Row(children: <Widget>[
                new Icon(Icons.book, color: Colors.white),
                new Text("Cetakan ke : ${bk.cetakanKe ?? kosong}",
                    style: _textcard, textAlign: TextAlign.center),
              ]),
              Row(children: <Widget>[
                new Icon(Icons.book, color: Colors.white),
                new Text("Jumlah Eksemplar : ${bk.jumlahEksemplar ?? kosong}",
                    style: _textcard, textAlign: TextAlign.center),
              ]),
              Row(children: <Widget>[
                new Icon(Icons.book, color: Colors.white),
                new Text("Tinggi buku : ${bk.tinggiBuku ?? kosong} cm",
                    style: _textcard, textAlign: TextAlign.center),
              ]),
            ],
          ),
          Container(
            height: 30.0,
          ),
        ],
      ),
    );
  }

  _listBukuTerkait(datum) {
    return FutureBuilder<List<Datum>>(
      future: _apiService.fetcBukuTerkait(datum.id),
      builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);

        return snapshot.hasData
            ? new ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: _bukuTerkait.length,
                itemBuilder: (context, index) {
                  return _listtile(index);
                })
            : _circularProcces();
      },
    );
  }

  _listtile(index) {
    return Column(
      children: <Widget>[
        Material(
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new DetailScreen3(
                        datums: _bukuTerkait[index],
                      )));
            },
            splashColor: Colors.amber,
            child: CustomVerticalList(
              judul: _bukuTerkait[index].judul,
            ),
          ),
        ),
      ],
    );
  }
}
