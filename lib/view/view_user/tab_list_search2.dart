import 'dart:async';

import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:opac_android_kp/Api/ApiService.dart';
import 'package:opac_android_kp/Class/Post.dart';
import 'package:opac_android_kp/view/view_user/detailScreen3.dart';
import 'package:toast/toast.dart';

class TabListSearch2 extends StatefulWidget {
  
  @override
  _TabListSearch2State createState() => _TabListSearch2State();
}

class _TabListSearch2State extends State<TabListSearch2> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();

  TextStyle _style = TextStyle(
    // fontFamily: "LatoBlack",
    fontFamily: "Poppins",
    // fontFamily: "BebasRegular",
  );

  ApiService _apiService = ApiService();
  static List<Datum> postsAdv = List<Datum>();
  List<Datum> _posts = List<Datum>();
  List<Datum> _postsPenerbit = List<Datum>();
  List<Datum> _postsPengarang = List<Datum>();

  TextEditingController editingController = TextEditingController();
  int page = 1;
  String text = "ehe";
  String kosong = "-";

  bool isAdv = false;
  bool isAda = false;
  bool isKosong = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
    });
    setState(() {
      print("refresh");
      editingController.clear();
      _posts.clear();
      _postsPenerbit.clear();
      _postsPengarang.clear();
      // items.clear();
      // print(items.length);
    });
    return completer.future.then<void>((_) {
      _scaffoldKey.currentState?.showSnackBar(SnackBar(
          content: const Text('Refresh complete'),
          action: SnackBarAction(
              label: 'RETRY',
              onPressed: () {
                _refreshIndicatorKey.currentState.show();
              })));
    });
  }

  List<Widget> createJudul(int count) {
    // Datum datum = _posts[index];
    List<Widget> items = List<Widget>();
    setState(() {
      // print("judul : ${items.length}");
      for (var i = 1; i <= count; i++) {
        items.add(Divider(
          color: Colors.blue,
          thickness: 2.0,
        ));
        items = (_posts.map((e) {
          return items != 0
              ? ListTile(
                  leading: Icon(Icons.receipt),
                  title: Text(e.judul),
                  onTap: () {
                    print(e.id);
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new DetailScreen3(
                              datums: e,
                            )));
                  },
                )
              : Text("data tdk tersedia");
        }).toList());
      }
      // print("judul : ${items.length}");
    });
    return items;
  }

  List<Widget> createPengarang(int count) {
    // Datum datum = _posts[index];
    List<Widget> items = List<Widget>();
    setState(() {
      for (var i = 1; i <= count; i++) {
        items = (_postsPengarang.map((e) {
          return items != 0
              ? ListTile(
                  leading: Icon(Icons.receipt),
                  title: Text(e.judul),
                  onTap: () {
                    print(e.id);
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new DetailScreen3(
                              datums: e,
                            )));
                  },
                )
              : Text("data tdk tersedia");
        }).toList());
      }
      // print("pengarang : ${items.length}");
    });
    return items;
  }

  List<Widget> createPenerbit(int count) {
    // Datum datum = _posts[index];
    List<Widget> items = List<Widget>();
    setState(() {
      for (var i = 1; i <= count; i++) {
        items = (_postsPenerbit.map((e) {
          return items != 0
              ? ListTile(
                  leading: Icon(Icons.receipt),
                  title: Text(e.judul),
                  onTap: () {
                    print(e.id);
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new DetailScreen3(
                              datums: e,
                            )));
                  },
                )
              : Text("data tdk tersedia");
        }).toList());
      }
      // print("penerbit : ${items.length}");
    });
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home:
          // TextFieldAlertDialog()
          Scaffold(
              resizeToAvoidBottomPadding: false,
              backgroundColor: (Color.fromARGB(255, 139, 215, 234)),
              body: LiquidPullToRefresh(
                  key: _refreshIndicatorKey,
                  onRefresh: _handleRefresh,
                  showChildOpacityTransition: true,
                  backgroundColor: (Color.fromARGB(255, 139, 215, 234)),
                  color: Colors.white,
                  child: Column(children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "Cari Buku",
                        style: TextStyle(
                            fontFamily: "LatoBlack",
                            fontSize: 27,
                            color: Colors.white,
                            decoration: TextDecoration.none),
                      ),
                    ),
                    _searchBar(),
                    RaisedButton(
                      onPressed: () {
                        _onPressed(context);
                      },
                      child: Text("Pencarian Spesifik"),
                    ),
                    Visibility(
                      child: _circularProcces(),
                      visible: isLoading,
                    ),
                    Visibility(
                      visible: isAdv,
                        child: ListView.builder(
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(postsAdv[index].judul),
                        );
                      },
                      itemCount: postsAdv.length,
                    )),
                    Visibility(
                      visible: isAda,
                      child: Expanded(
                        child: ListView(
                          children: <Widget>[
                            ExpansionTile(
                                initiallyExpanded: true,
                                leading: Icon(Icons.library_books),
                                backgroundColor: Colors.white,
                                title: Text(
                                  "Judul",
                                  style: _style,
                                ),
                                children: createJudul(_posts.length)),
                            ExpansionTile(
                                leading: Icon(Icons.bookmark),
                                backgroundColor: Colors.white,
                                title: Text("Penerbit", style: _style),
                                children:
                                    createPenerbit(_postsPenerbit.length)),
                            ExpansionTile(
                                leading: Icon(Icons.person),
                                backgroundColor: Colors.white,
                                title: Text("Pengarang", style: _style),
                                children:
                                    createPengarang(_postsPengarang.length))
                          ],
                        ),
                      ),
                    ),
                    Visibility(child: _notFound(), visible: isKosong),
                    //   onRefresh: _handleRefresh)
                  ]))),
    );
  }

  void _onPressed(BuildContext context) {
    showDialog(
        context: context,
        builder: (_) {
          return MyDialog();
        });
  }

  _notFound() {
    return Text(
      "Oops, Data tidak ditemukan",
      style: TextStyle(fontSize: 20, color: Colors.red),
    );
  }

  _circularProcces() {
    return Align(
      alignment: Alignment.topCenter,
      child: CircularProgressIndicator(),
    );
  }

  _searchBar() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: TextField(
            // style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                labelText: "Search",
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25.0)),
                    borderSide: BorderSide(color: Colors.white))),
            controller: editingController,
            onChanged: (textt) {
              setState(() {
                isLoading = false;
                if (textt.isEmpty) {
                  isKosong = false;

                  print("teks kosong");
                  // _posts.clear();
                  // _postsPenerbit.clear();
                  // _postsPengarang.clear();
                }
              });
            },
            onSubmitted: (textt) {
              isLoading = true;
              print(textt);
              _apiService.check().then((value) {
                if (!value) {
                  Toast.show("Lost Connection :(", context,
                      duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                } else {
                  if (textt.isEmpty) {
                    isLoading = false;
                  } else {
                    setState(() {
                      isLoading = true;
                      _posts.clear();
                      _postsPenerbit.clear();
                      _postsPengarang.clear();
                      _apiService.searchJudul(textt).then((value) {
                        setState(() {
                          isLoading = false;
                          var judul = value['judul']; //untuk data judul
                          var pengarang =
                              value['pengarang']; //untuk data pengarang
                          var penerbit =
                              value['penerbit']; //untuk data penerbit

                          for (var dataJson in judul) {
                            _posts.add(Datum.fromJson(dataJson));
                          }
                          for (var dataJson in pengarang) {
                            _postsPengarang.add(Datum.fromJson(dataJson));
                          }
                          for (var dataJson in penerbit) {
                            _postsPenerbit.add(Datum.fromJson(dataJson));
                          }
                          setState(() {
                            if ((_posts.length == 0 &&
                                _postsPenerbit.length == 0 &&
                                _postsPengarang.length == 0)) {
                              isAda = false;
                              isKosong = !isAda;
                            }
                            _posts.length == 0 ? "no data" : {isAda = true};
                            _postsPenerbit.length == 0
                                ? "no data penerbit"
                                : isAda = true;
                            _postsPengarang.length == 0
                                ? "no data pengarang"
                                : isAda = true;
                          });
                        });
                      });
                    });
                  }
                }
              });
            },
          ),
        ),
      ],
    );
  }

  _listtile(index) {
    return Card(
      margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 2.0),
      color: Colors.white,
      child: ListTile(
        trailing: Icon(Icons.arrow_forward_ios),
        contentPadding: EdgeInsets.only(left: 20.0, right: 10.0),
        title: Text(postsAdv[index].judul ?? kosong),
        leading: new Icon(Icons.library_books),
        onTap: () => Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) =>
                new DetailScreen3(datums: postsAdv[index]))),
      ),
    );
  }
}

class MyDialog extends StatefulWidget {
  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  ApiService _apiService = ApiService();
  TabListSearch2 search2 = TabListSearch2();
  TextEditingController _textFieldJudul = TextEditingController();
  TextEditingController _textFieldPenerbit = TextEditingController();
  TextEditingController _textFieldPengarang = TextEditingController();
  TextEditingController _textFieldTahun = TextEditingController();
  bool isEnabledJudul = true;
  bool isEnabledPenerbit = false;
  bool isEnabledPengarang = false;
  bool isEnabledTahun = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Pencarian Spesifik"),
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                Switch(
                  value: isEnabledJudul,
                  onChanged: (bool value) {
                    setState(() {
                      isEnabledJudul = value;
                    });
                  },
                ),
                Text("Judul")
              ],
            ),
            TextField(
              controller: _textFieldJudul,
              enabled: isEnabledJudul,
              decoration: InputDecoration(hintText: "judul"),
            ),
            Row(
              children: <Widget>[
                Switch(
                  value: isEnabledPenerbit,
                  onChanged: (bool value) {
                    setState(() {
                      isEnabledPenerbit = value;
                    });
                  },
                ),
                Text("Penerbit")
              ],
            ),
            TextField(
              controller: _textFieldPenerbit,
              enabled: isEnabledPenerbit,
              decoration: InputDecoration(hintText: "penerbit"),
            ),
            Row(
              children: <Widget>[
                Switch(
                  value: isEnabledPengarang,
                  onChanged: (bool value) {
                    setState(() {
                      isEnabledPengarang = value;
                    });
                  },
                ),
                Text("Pengarang")
              ],
            ),
            TextField(
              controller: _textFieldPengarang,
              enabled: isEnabledPengarang,
              decoration: InputDecoration(hintText: "pengarang"),
            ),
            Row(
              children: <Widget>[
                Switch(
                  value: isEnabledTahun,
                  onChanged: (bool value) {
                    setState(() {
                      isEnabledTahun = value;
                    });
                  },
                ),
                Text("Tahun")
              ],
            ),
            TextField(
              controller: _textFieldTahun,
              enabled: isEnabledTahun,
              decoration: InputDecoration(hintText: "tahun"),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
            child: Text(
              'CANCEL',
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        FlatButton(
            child: Text('CARI'),
            onPressed: () {
              setState(() {
                print(_textFieldJudul.text.toString());
                _apiService.advSearch(
                    _textFieldJudul.text.toString(),
                    _textFieldPenerbit.text.toString(),
                    _textFieldPengarang.text.toString(),
                    _textFieldTahun.text.toString());
              });
              Navigator.of(context).pop();
            }),
      ],
    );
  }
}

// Create the Widget for the row
