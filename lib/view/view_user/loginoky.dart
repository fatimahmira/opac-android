import 'dart:convert';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:opac_android_kp/Api/ApiService.dart';
import 'package:opac_android_kp/view/view_admin/homepage_admin.dart';
import 'package:opac_android_kp/view/view_pimpinan/homepage_pimpinan.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  ApiService _apiService = ApiService();
  double getLebarLayar(BuildContext context) =>
      MediaQuery.of(context).size.width;

  double getTinggiLayar(BuildContext context) =>
      MediaQuery.of(context).size.height;

  final FocusNode _usernameFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _buttonFocus = FocusNode();

//  form
  final _formkey = GlobalKey<FormState>();

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isUserNonValid = false;
  bool isLogin = true;
  bool isLoading = false;
  String user = '';
  String pass = '';
  String nonValid = "";

  @override
  void initState() {
    _check();
    // TODO: implement initState
    super.initState();
  }

  Future<bool> _check() async {
    bool result = await DataConnectionChecker().hasConnection;
    if (result == true) {
      return true;
    } else {
      print('No internet :( Reason:');
      return false;
      // print(DataConnectionChecker().lastTryResults);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 161, 211, 255),
        centerTitle: true,
        title: Text(
          'OPAC',
          style: TextStyle(
            fontFamily: "AdigianaUI",
            fontSize: 40.0,
          ),
        ),
      ),
      body:
          //bagian sebelum column form login ada pattern , sedang  download xd,
          Container(
        color: Colors.transparent,
        child: ListView(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 4),
                child: Center(
                  child: Text("Login sebagai Admin",
                      style: TextStyle(
                        fontFamily: "StabilloMedium",
                        fontSize: 48.0,
                        color: Color.fromARGB(255, 69, 168, 255),
                      )),
                )),
            Form(
              key: _formkey,
              child: Column(
                children: <Widget>[
                  _username(context),
                  _password(context),
                  _textalert(context),
                  _login(context),
                  // RaisedButton(
                  //     child: Text("Masuk"),
                  //     onPressed: () {
                  //       _submit();
                  //     }),
                  // RaisedButton(
                  //     child: Text("Masuk Pimpinan"),
                  //     onPressed: () {
                  //       if (_formkey.currentState.validate()) {
                  //         setState(() {
                  //           Navigator.of(context).pushReplacement(
                  //               MaterialPageRoute(
                  //                   builder: (BuildContext context) =>
                  //                       HomePagePimpinan()));
                  //         });
                  //       }
                  //     })
                ],
              ),
            ),
            _loading(),
          ],
        ),
      ),
    );
  }

  String validasiPass(String value) {
    if (value.isEmpty) {
      return 'Password cannot be empty';
    }
    return null;
  }

  String validasiUser(String value) {
    if (value.isEmpty) {
      return 'Email cannot be empty';
    }
  }

  _textalert(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all((isUserNonValid) ? 16.0 : 0.0),
        child: Text(
          (isUserNonValid) ? nonValid : "",
          style: TextStyle(color: Colors.red),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Container _username(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 20.0,
        left: 16.0,
        right: 16.0,
      ),
      margin: EdgeInsets.only(
        bottom: 16.0,
      ),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(9.0),
          ),
          labelText: "Username",
          labelStyle: TextStyle(
            color: Colors.black,
          ),
          hintText: "Masukkan Username Anda...",
          hintStyle: TextStyle(color: Colors.grey),
          contentPadding: EdgeInsets.fromLTRB(24.0, 16.0, 24.0, 16.0),
        ),
        maxLines: 1,
        controller: usernameController,
        validator: validasiUser,
        textInputAction: TextInputAction.next,
        focusNode: _usernameFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChanged(context, _usernameFocus, _passwordFocus);
        },
        onSaved: (String value) {
          user = value;
        },
      ),
    );
  }

  Container _password(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 16.0,
        right: 16.0,
      ),
      child: TextFormField(
        obscureText: true,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(9.0),
          ),
          labelText: "Password",
          labelStyle: TextStyle(
            color: Colors.black,
          ),
          hintText: "Masukkan Password Anda...",
          hintStyle: TextStyle(color: Colors.grey),
          contentPadding: EdgeInsets.fromLTRB(24.0, 16.0, 24.0, 16.0),
        ),
        maxLines: 1,
        controller: passwordController,
        textInputAction: TextInputAction.done,
        focusNode: _passwordFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChanged(context, _passwordFocus, _buttonFocus);
        },
        validator: validasiPass,
        onSaved: (String value) {
          pass = value;
        },
      ),
    );
  }

  Padding _login(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 45.0, right: 45.0, bottom: 15.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 50,
        child: Container(
          child: Material(
            borderRadius: BorderRadius.circular(9.0),
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(9.0),
              splashColor: Colors.grey,
              onTap: () {
                // setState(() {
                //   isLoading = true;
                // });
                login(usernameController.text, passwordController.text);
              },
              focusNode: _buttonFocus,
              child: Center(
                child: Text(
                  "LOGIN",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w700),
                ),
              ),
            ),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9.0),
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 161, 211, 255),
                Color.fromARGB(255, 122, 193, 255),
                Color.fromARGB(255, 82, 174, 255),
              ],
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
            ),
          ),
        ),
      ),
    );
  }

  _loading() {
    return Visibility(
      child: Center(child: new CircularProgressIndicator()),
      visible: isLoading,
    );
  }

  void login(String email, String pass) async {
    if (_formkey.currentState.validate()) {
      setState(() {
        isLoading = true;
      });
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      _check().then((value) {
        if (value == false) {
          setState(() {
            isLoading = false;
          });
          Toast.show("Lost Connection :(", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        } else {
          _apiService.login(email, pass).then((value) {
            var respon = json.decode(value.body);

            var postsJson = respon['data'];
            var role = postsJson['role_id'];
            print(role);

            if (value.statusCode == 200) {
              setState(() {
                isLoading = false;
                if (role == 1) {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => HomePagePimpinan()));
                  print("masuk pimpinan");
                } else if (role == 2) {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => HomePageAdmin()));
                  print("masuk admin");
                } else {
                  Toast.show("Coba Lagi deh!", context,
                      duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                  Navigator.pop(context);
                }
                sharedPreferences.setBool("isLogin", isLogin);
                sharedPreferences.setInt("role_account", role);
                print(sharedPreferences.getBool("isLogin"));
                print(sharedPreferences.getInt("role_account"));
              });
            } else if (value.statusCode == 401) {
              setState(() {
                isLoading = false;
              });
              Toast.show("Email/Password salah", context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            } else {
              Toast.show("Coba Lagi deh!", context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            }
          });
        }
      });
    }
  }

  void _submit() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (_formkey.currentState.validate()) {
      setState(() {
        sharedPreferences.setBool("isLogin", isLogin);
        sharedPreferences.setInt("role_account", 2);
        print(isLogin);
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => HomePageAdmin()));
      });
    }
  }

  void _fieldFocusChanged(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
