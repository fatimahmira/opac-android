import 'package:flutter/material.dart';
import 'package:opac_android_kp/Api/ApiService.dart';
import 'package:opac_android_kp/view/view_user/loginoky.dart';

class TabListSubjek extends StatefulWidget {
  @override
  _TabListSubjekState createState() => _TabListSubjekState();
}

class _TabListSubjekState extends State<TabListSubjek> {
  ApiService _apiService = new ApiService();
  static const TextStyle _style = TextStyle(
    fontFamily: "Poppins",
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          child: ListTile(
            leading: Icon(Icons.person),
            title: Text("Masuk sebagai Admin", style: _style),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {
              print("hahaha");
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new LoginPage()));
            },
          ),
        ),
        Expanded(
            child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom:40.0),
                  child: Text(
                    "OPAC from Digistlab",
                    style: TextStyle(
                        fontFamily: "Poppins",
                        fontSize: 17,
                        color: Colors.grey,
                        decoration: TextDecoration.none),
                  ),
                )))
      ],
    );
  }
  
}
