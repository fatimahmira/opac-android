import 'dart:async';

import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:opac_android_kp/Api/ApiService.dart';
import 'package:opac_android_kp/Class/Post.dart';
import 'package:toast/toast.dart';

class TabPelaporan extends StatefulWidget {
  @override
  _TabPelaporanState createState() => _TabPelaporanState();
}

class _TabPelaporanState extends State<TabPelaporan>
    with SingleTickerProviderStateMixin {
  ApiService _apiService = ApiService();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();
  Animation<double> animation;
  Animation<double> animation1;
  Animation<double> animation2;
  Animation<double> animation3;
  AnimationController _animationController;
  List<Datum> _baru = List<Datum>();
  List<Datum> _hapus = List<Datum>();
  int jumlah = 0;
  int judul = 0;
  var baru;
  var hapus;
  String jumlah1 = "0";
  String judul1 = "0";
  String baru1 = "0";
  String hapus1 = "0";

  static const TextStyle _textcardbesar = TextStyle(
      fontSize: 20.0,
      fontWeight: FontWeight.w900,
      color: Colors.white,
      // fontFamily: "StabilloMedium",
      shadows: [
        Shadow(blurRadius: 5.0, color: Colors.black, offset: Offset(2.0, 2.0))
      ]);
  static const TextStyle _textcardkecil = TextStyle(
    fontSize: 20.0,

    fontWeight: FontWeight.w900,
    color: Colors.grey,
    // fontFamily: "StabilloMedium",
  );

  @override
  void initState() {
    _apiService.check().then((value) {
      if (!value) {
        Toast.show("Lost Connection :(", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        dispose();
      } else {
         _apiService.fetchPimpinan().then((value) {
      setState(() {
        jumlah = value['jumlah'];
        judul = value['judul'];
        baru = value['baru'];
        hapus = value['hapus'];
      });
      _animationController = AnimationController(
          duration: const Duration(seconds: 1), vsync: this);

      animation = Tween<double>(begin: 0, end: jumlah.toDouble())
          .animate(_animationController)
            ..addListener(() {
              setState(() {
                jumlah1 = animation.value.toStringAsFixed(0);
                
              });
            });
      animation1 = Tween<double>(begin: 0, end: judul.toDouble())
          .animate(_animationController)
            ..addListener(() {
              setState(() {
                judul1 = animation1.value.toStringAsFixed(0);
              });
            });
      animation2 = Tween<double>(begin: 0, end: baru.toDouble())
          .animate(_animationController)
            ..addListener(() {
              setState(() {
                baru1 = animation2.value.toStringAsFixed(0);
              });
            });
      animation3 = Tween<double>(begin: 0, end: hapus.toDouble())
          .animate(_animationController)
            ..addListener(() {
              setState(() {
                hapus1 = animation3.value.toStringAsFixed(0);
              });
            });

      _animationController.forward();
      
      });
    }});

    super.initState();
  }

  @override
  void dispose() {
    // _animationController.stop(canceled: true);
    _animationController.dispose();

    super.dispose();
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
    });

    return completer.future.then<void>((_) {
      _scaffoldKey.currentState?.showSnackBar(SnackBar(
          content: const Text('Refresh complete'),
          action: SnackBarAction(
              label: 'RETRY',
              onPressed: () {
                _refreshIndicatorKey.currentState.show();
              })));
    });
  }

  List<Widget> getBaru(int count) {
    // Datum datum = _posts[index];
    List<Widget> items = List<Widget>();
    setState(() {
      // print("judul : ${items.length}");
      for (var i = 1; i <= count; i++) {
        items = (_baru.map((e) {
          return ListTile(
            leading: Icon(Icons.receipt),
            title: Text(e.judul),
          );
        }).toList());
      }
      // print("judul : ${items.length}");
    });
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          backgroundColor: Colors.grey[200],
          resizeToAvoidBottomPadding: false,
          body: LiquidPullToRefresh(
              key: _refreshIndicatorKey,
              onRefresh: _handleRefresh,
              showChildOpacityTransition: false,
              color: Colors.white,
              backgroundColor: Colors.grey[200],
              child: ListView(children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      "Laporan Buku",
                      style: TextStyle(
                          fontFamily: "StabilloMedium",
                          fontSize: 50,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                          shadows: [
                            Shadow(
                                blurRadius: 10.0,
                                color: Colors.black,
                                offset: Offset(5.0, 5.0))
                          ]),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: 35.0, left: 100.0, right: 100.0),
                  child: Container(
                    padding: EdgeInsets.all(25),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Total Buku",
                          style: _textcardkecil,
                        ),
                        Text(
                          "$jumlah1 Buku",
                          style: _textcardbesar,
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[500],
                              blurRadius: 15.0,
                              offset: Offset(4.0, 4.0),
                              spreadRadius: 1.0),
                          BoxShadow(
                              color: Colors.white,
                              blurRadius: 15.0,
                              offset: Offset(-4.0, -4.0),
                              spreadRadius: 1.0),
                        ]),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 35.0, left: 100.0, right: 100.0),
                  child: Container(
                    padding: EdgeInsets.all(25),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Judul Buku",
                          style: _textcardkecil,
                        ),
                        Text(
                          "${judul1} Buku",
                          style: _textcardbesar,
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[500],
                              blurRadius: 15.0,
                              offset: Offset(4.0, 4.0),
                              spreadRadius: 1.0),
                          BoxShadow(
                              color: Colors.white,
                              blurRadius: 15.0,
                              offset: Offset(-4.0, -4.0),
                              spreadRadius: 1.0),
                        ]),
                  ),
                ),

                Padding(
                  padding:
                      EdgeInsets.only(top: 35.0, left: 100.0, right: 100.0),
                  child: Container(
                    padding: EdgeInsets.all(25),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Buku Baru",
                          style: _textcardkecil,
                        ),
                        Text(
                          "$baru1 Buku",
                          style: _textcardbesar,
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[500],
                              blurRadius: 15.0,
                              offset: Offset(4.0, 4.0),
                              spreadRadius: 1.0),
                          BoxShadow(
                              color: Colors.white,
                              blurRadius: 15.0,
                              offset: Offset(-4.0, -4.0),
                              spreadRadius: 1.0),
                        ]),
                  ),
                ),

                Padding(
                  padding:
                      EdgeInsets.only(top: 35.0, left: 100.0, right: 100.0, bottom: 50.0),
                  child: Container(
                    padding: EdgeInsets.all(25),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Buku yang dihapus",
                          style: _textcardkecil,
                        ),
                        Text(
                          "$hapus1 Buku",
                          style: _textcardbesar,
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[500],
                              blurRadius: 15.0,
                              offset: Offset(4.0, 4.0),
                              spreadRadius: 1.0),
                          BoxShadow(
                              color: Colors.white,
                              blurRadius: 15.0,
                              offset: Offset(-4.0, -4.0),
                              spreadRadius: 1.0),
                        ]),
                  ),
                ),
                // ExpansionTile(
                //     initiallyExpanded: true,
                //     leading: Icon(Icons.library_books),
                //     backgroundColor: Colors.white,
                //     title: Text(
                //       "Judul",
                //     ),
                //     children: getBaru(_baru.length)),
                // Padding(
                //   padding: const EdgeInsets.only(
                //       top: 30.0, bottom: 15.0, left: 45.0, right: 45.0),
                //   child: Container(
                //     decoration: BoxDecoration(
                //         color: Colors.white,
                //         borderRadius: BorderRadius.circular(10),
                //         boxShadow: [
                //           BoxShadow(
                //               color: Colors.grey[500],
                //               blurRadius: 15.0,
                //               offset: Offset(3.0, 3.0),
                //               spreadRadius: 1.0),
                //           BoxShadow(
                //               color: Colors.white,
                //               blurRadius: 15.0,
                //               offset: Offset(-4.0, -4.0),
                //               spreadRadius: 1.0)
                //         ]),
                //     child: ExpansionCard(
                //       borderRadius: 15.0,
                //       background: Image.asset(
                //         "images/pimpinanbaru2.PNG",
                //         fit: BoxFit.contain,
                //       ),
                //       backgroundColor: Colors.transparent,
                //       margin:
                //           EdgeInsets.only(top: 25.0, left: 45.0, right: 45.0),
                //       title: Text(
                //         "Judul Buku Baru",
                //         style: _textcardkecil,
                //       ),
                //       initiallyExpanded: true,
                //       children: <Widget>[
                //         Text("(data)"),
                //         Padding(
                //           padding: const EdgeInsets.all(20.0),
                //           child: Text("20693 Pcs", style: _textcardbesar),
                //         )
                //       ],
                //     ),
                //   ),
                // ),
                // Padding(
                //   padding: const EdgeInsets.only(
                //       top: 10.0, bottom: 15.0, left: 45.0, right: 45.0),
                //   child: Container(
                //     decoration: BoxDecoration(
                //         color: Colors.white,
                //         borderRadius: BorderRadius.circular(10),
                //         boxShadow: [
                //           BoxShadow(
                //               color: Colors.grey[500],
                //               blurRadius: 15.0,
                //               offset: Offset(3.0, 3.0),
                //               spreadRadius: 1.0),
                //           BoxShadow(
                //               color: Colors.white,
                //               blurRadius: 15.0,
                //               offset: Offset(-4.0, -4.0),
                //               spreadRadius: 1.0)
                //         ]),
                //     child: ExpansionCard(
                //       borderRadius: 15.0,
                //       background: new Image(
                //         alignment: Alignment.bottomRight,
                //         image: new AssetImage("images/pimpinanbaru.PNG"),
                //         fit: BoxFit.contain,
                //       ),
                //       backgroundColor: Colors.transparent,
                //       // Image.asset("images/IMGpimpinan.JPG", fit: BoxFit.,),
                //       margin:
                //           EdgeInsets.only(top: 25.0, left: 45.0, right: 45.0),
                //       title: Text(
                //         "Judul Buku",
                //         style: _textcardkecil,
                //       ),
                //       initiallyExpanded: false,
                //       children: <Widget>[
                //         Padding(
                //           padding: const EdgeInsets.all(20.0),
                //           child: Text(
                //             "8413 judul",
                //             style: _textcardbesar,
                //           ),
                //         )
                //       ],
                //     ),
                //   ),
                // ),
              ]))),
    );
  }
}

// Create the Widget for the row
