import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:opac_android_kp/view/view_admin/tab_list_subjek.dart';
import 'package:opac_android_kp/view/view_pimpinan/laporan.dart';
import 'package:opac_android_kp/view/view_pimpinan/tab_list_subjek.dart';

class HomePagePimpinan extends StatefulWidget {
  @override
  _HomePagePimpinanState createState() => _HomePagePimpinanState();
}

class _HomePagePimpinanState extends State<HomePagePimpinan> {
  // AnimationController _controller;
  // Widget animationBuilder;

  // void forward(Duration duration) {
  //   _controller.duration = duration;
  //   _controller.forward();
  // }

  int _selectedIndex = 0;
  static const TextStyle options =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 30);
  List<Widget> _widgetoptions = <Widget>[
    
    TabPelaporan(),
    TabListSubjekPimpinan(),
  ];

// when  the item on tapped this function will active
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/homepagePimpinan': (BuildContext context) {
          return HomePagePimpinan();
        }
      },
          home:  Scaffold(
            
        appBar: AppBar(
          backgroundColor: 
                Color.fromARGB(255, 82, 174, 255),
          centerTitle: true,
          title: const Text(
            'OPAC',
            style: TextStyle(
              fontFamily: "AdigianaUI",
              fontSize: 30.0,
            ),
          ),
        ),
        body: Center(child: _widgetoptions.elementAt(_selectedIndex)),
        bottomNavigationBar: FancyBottomNavigation(
          tabs: [
            TabData(iconData: Icons.note, title: "Laporan"),
            TabData(iconData: Icons.account_circle, title: "Akun"),
          ],
          onTabChangedListener: (position) {
            setState(() {
              _selectedIndex = position;
            });
          },
          circleColor: 
                Color.fromARGB(255, 82, 174, 255),
    )));
  
  }
}
