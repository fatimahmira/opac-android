import 'package:flutter/material.dart';
import 'package:opac_android_kp/view/view_user/homepage_user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';

class TabListSubjekPimpinan extends StatefulWidget {
  @override
  _TabListSubjekPimpinanState createState() => _TabListSubjekPimpinanState();
}

class _TabListSubjekPimpinanState extends State<TabListSubjekPimpinan> {
  static const TextStyle _style = TextStyle(
    fontFamily: "Poppins",
  );

  void _hapusCache() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove("isLogin");
    sharedPreferences.remove("role_account");
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          child: ListTile(
            leading: Icon(Icons.person),
            title: Text("Keluar Dari Admin", style: _style),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {
              print("hahaha");
              SweetAlert.show(context,
                  title: "Anda yakin ingin keluar?",
                  style: SweetAlertStyle.confirm,
                  showCancelButton: true, onPress: (bool isConfirm) {
                if (isConfirm) {
                  _hapusCache();
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => new HomePage()));
                }
              });
            },
          ),
        ),

        Expanded(
            child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 40.0),
                  child: Text(
                    "OPAC from Digistlab",
                    style: TextStyle(
                        fontFamily: "Poppins",
                        fontSize: 17,
                        color: Colors.grey,
                        decoration: TextDecoration.none),
                  ),
                )))
      ],
    );
  }
}
