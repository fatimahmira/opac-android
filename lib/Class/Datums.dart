import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Post.dart';

Datums datumsFromJson(String str) => Datums.fromJson(json.decode(str));

String datumsToJson(Datums data) => json.encode(data.toJson());

class Datums {
    dynamic status;
    dynamic message;
    Datum data;

    Datums({
        this.status,
        this.message,
        this.data,
    });

    Widget _buildTiles(Datum root) {
    if (root.judul.isEmpty) {
      return ListTile(
        title: Text(root.judul),
      );
    }
    return ExpansionTile(
      key: PageStorageKey<Datum>(root),
      title: Text(root.judul),
    );
  }

    factory Datums.fromJson(Map<String, dynamic> json) => Datums(
        status: json["status"],
        message: json["message"],
        data: Datum.fromJson(json["buku"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "buku": data.toJson(),
    };
}