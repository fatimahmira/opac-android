import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:opac_android_kp/view/view_admin/homepage_admin.dart';
import 'package:opac_android_kp/view/view_pimpinan/homepage_pimpinan.dart';
import 'package:opac_android_kp/view/view_user/homepage_user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _visible = false;
  double _opacity = 0.0;
  bool isLogin = false;
  // double judul = MediaQuery.of(context
  @override
  void initState() {
    super.initState();

    Timer(Duration(milliseconds: 1000), () {
      doit();

      Timer(
          Duration(seconds: 3),
          () =>
          
          isAdmin()
      );
    });
  }

  void doit() {
    setState(() {
      _opacity = _opacity == 0.0 ? 1.0 : 0.0;
    });
  }

  void isAdmin() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
    print(isLogin);
    isLogin = sharedPreferences.getBool("isLogin");
    int role = sharedPreferences.getInt("role_account");
    if(isLogin==true){
      if (role==1) {
         Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) =>
              HomePagePimpinan() //disini tempaat ganti apa yg mau ditampilin abis splash screen
          ));
      } else  if (role==2) {
         Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) =>
              HomePageAdmin() //disini tempaat ganti apa yg mau ditampilin abis splash screen
          ));
      } else {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) =>
              HomePage() //disini tempaat ganti apa yg mau ditampilin abis splash screen
          ));
      }
    } else{
       Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) =>
              HomePage() //disini tempaat ganti apa yg mau ditampilin abis splash screen
          ));
    }
     
    
    });
    
      
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            image: new DecorationImage(
                image: new AssetImage("images/pustaka.JPG"),
                fit: BoxFit.cover)),
        child: new BackdropFilter(
          filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height / 3),
            child: AnimatedOpacity(
              opacity: _opacity,
              duration: Duration(milliseconds: 1500),
              child: Column(
                children: <Widget>[
                  Image.asset('images/logoOPAC.PNG'),
                  Text(
                    'STAI Auliaurrasyidin',
                    style: TextStyle(
                      fontFamily: "AdigianaUI",
                      fontSize: 30.0,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
